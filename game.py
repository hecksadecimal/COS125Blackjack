import random
import itertools
import time
import sys

# immense credit goes to Mike McGowan, whose decade-old code I found in the
# bowels of the internet, and which helped me very much when I was stuck an
    # hour before this assignment's due time.

deck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10] * 4 # 4 deck shoe, royals treated as 10 per the usual
bobo = []
player = []
balance = 5000

def showOfHands():
    hand = 0
    for i in player: hand += i
    print "Bobo shows %d" % bobo[0]
    print "Your hand totals %d (%s)" % (hand, player)

def initialCards():
    for i in range(2):
        dealToBobo = deck[random.randint(1, len(deck)-1)]
        dealToPlayer = deck[random.randint(1, len(deck)-1)]
        bobo.append(dealToBobo) # add contents of Bobo's deal to his hand
        player.append(dealToPlayer) # same with Player
        deck.pop(dealToBobo) # "pop" Bobo's cards off the deck stack
        deck.pop(dealToPlayer) # same for Player's cards

def clearScreen():
    print '\n'*100

def playGame():
    global balance
    print "Your current balance is: ",balance
    if balance < 1:
        print "You are bankrupt!\nBobo shrieks with joy!"
        sys.exit()
    bet = input("Please enter your bet amount: ")
    balance -= bet
    if balance < 0:
        print "That is invalid!"
        balance += bet
        playGame()
    print "Your new balance is: ", balance
    time.sleep(3)
    initialCards()
    action = raw_input("What do you do\n[H]it me! [S]tand, [Q]uit: ").lower()
    while action != "q":
        showOfHands()
        action = raw_input("What do you do?\n[H]it me! [S]tand, [Q]uit: ").lower()
        clearScreen()
        if action == 'h':
            dealToPlayer = deck[random.randint(1, len(deck)-1)]
            global player
            player.append(dealToPlayer)
            deck.pop(dealToPlayer)
            hand = 0
            for i in bobo: hand += i # dealer goes after player
            if hand < 18: # dealer stands on 17
                dealToBobo = deck[random.randint(1, len(deck)-1)]
                bobo.append(dealToBobo)
                deck.pop(dealToBobo)
            hand = 0
            for i in player: hand += i
            if hand > 21:
                print "Bust! You forfeit your wager!\nBobo cackles with glee!"
                player = [] # clear player hand
                bobo = [] # clear dealer hand
                initialCards()
            hand = 0
            for i in bobo: hand += i
            if hand > 21:
                print "Bobo busts! He howls in frustration!\nYou receive double your wager!\n"
                bet *= 2
                global balance
                balance += bet
                print "Your new balance is: ", balance
                player = []
                bobo = []
                initialCards()
        elif action == 's':
            boboHand = 0 #dealer's hand total
            playerHand = 0 #same for player
            global bobo
            for i in bobo: boboHand += i
            for i in player: playerHand += i
            if playerHand > boboHand:
                print "You win! Bobo bursts into tears!\nYou receive double your wager!\n"
                bet *= 2
                global balance
                balance += bet
                print "Your new balance is: ", balance
                bobo = []
                player = []
                initialCards()
            else:
                print "You lose! Bobo flails with joy!\nYou lose your wager!\n"
                bobo = []
                player = []
                initialCards()
        else:
            if action == 'q':
                seeYa = raw_input("Goodbye. Press any button to leave.")
            else:
                print "Invalid!"
clearScreen()
print "Welcome to The Grand Lewicki Casino! \n\nUnfortunately, our facilities are currently undergoing renovations, \nand our only available game is Blackjack."
print "Perhaps yet more unfortunately, our only accredited dealer was\nstruck down by a crazed heroin addict who was\n\"Lookin' for the perfect damn rock!\"\nWhile he recovers, he has been replaced by Bobo,\nthe trained orangutan, who can sort of \nplay the dealer role for you.\n\nBobo does not understand how to\nprovide insurance, split, or double down.\nIn other words, he only knows the basics.\n\nAs compensation for this inconvenience,\nyou will begin with a balance of 5,000 Casino Coins!"
print "So, with that out of the way...\n"
time.sleep(5)
beginAnswer = raw_input("Ready to play some Blackjack? (y/n)\n")
if beginAnswer.lower() == "y" or beginAnswer.lower() == "yes":
    print "\nGood! Bobo, shuffle up and deal!\n"
    time.sleep(1)
    print "*shuffle* *shuffle* *shuffle*"
    time.sleep(3)
    clearScreen()
    playGame()
else:
    print "\nWhy did you come here, then? Begone with ye!\n"
